# XMPP Interop Testing Gitlab Component

Gitlab CI component that performs XMPP interoperability tests on an XMPP domain.

The component is imported with inputs to provide context. It then provides a hidden job named `.xmpp-interop-tests` that can be extended.

## Test Account Provisioning

The job will typically execute various tests. Each test will use a fresh set of XMPP user accounts. These are automatically provisioned by the testing framework. They will be removed after the test execution.

The following strategies for test account provisioning are supported:

- By default, the test accounts are provisioned using XMPP's "In-band Registration" functionality (as defined in [XEP-0077](https://xmpp.org/extensions/xep-0077.html)).
- Alternatively, test accounts can be provisioned using XMPP 'Ad-hoc commands', as specified in [XEP-0133: Service Administration](https://xmpp.org/extensions/xep-0133.html). To enable this way of provisioning, the import's inputs must include the optional `adminAccountUsername` and `adminAccountPassword` inputs (as documented below).

## Inputs

The job can be configured using the following inputs:

- `host`: IP address or DNS name of the XMPP service to run the tests on. Default value: `127.0.0.1`
- `domain`: the XMPP domain name of server under test. Default value: `example.org`
- `timeout`: the amount of milliseconds after which an XMPP action (typically an IQ request) is considered timed out. Default value: `60000` (one minute)
- `adminAccountUsername`: _(optional)_ The account name of a pre-existing user that is allowed to create other users, per [XEP-0133](https://xmpp.org/extensions/xep-0133.html). If not provided, in-band registration ([XEP-0077](https://xmpp.org/extensions/xep-0077.html)) will be used to provision accounts.
- `adminAccountPassword`: _(optional)_ The password of the admin account.
- `disabledTests`: _(optional)_ A comma-separated list of tests that are to be skipped. For example: `EntityCapsTest,SoftwareInfoIntegrationTest`
- `disabledSpecifications`: _(optional)_ A comma-separated list of specifications (not case-sensitive) that are to be skipped. For example: `XEP-0045,XEP-0060`
- `enabledTests`: _(optional)_ A comma-separated list of tests that are the only ones to be run. For example: `EntityCapsTest,SoftwareInfoIntegrationTest`
- `enabledSpecifications`: _(optional)_ A comma-separated list of specifications (not case-sensitive) that are the only ones to be run. For example: `XEP-0045,XEP-0060`

## Basic Configuration

```yaml
include:
  - component: gitlab.com/xmpp-interop-testing/xmpp-interop-testing-gitlab-component/interop-testing@v1.4.0
    inputs:
      domain: 'shakespeare.lit'
      adminAccountUsername: 'juliet'
      adminAccountPassword: 'O_Romeo_Romeo!'

compliance-tests:
  extends: .xmpp-interop-tests
  stage: test
```

## Usage Example

It is expected that this job is used in a continuous integration flow that creates a new build of the XMPP server that is to be the subject of the tests.

Very generically, the job is expected to be part of such a flow in this manner:

1. Compile and build server software
2. Start server and run test job

Given the isolation of jobs in GitLab, the start and running of tests must be combined, normally by starting the server in the `before_script`. This could look something like the flow below. Note that all but the third step in this flow are placeholders or use placeholder config. They need to be replaced by steps that are specific to the server that is being built and tested.

```yaml
maven-build:
  stage: build
  script: ./mvnw package

include:
  - component: gitlab.com/xmpp-interop-testing/xmpp-interop-testing-gitlab-component/interop-testing@v1.4.0
    inputs:
      domain: 'shakespeare.lit'
      host: '127.0.0.1'
      adminAccountUsername: 'juliet'
      adminAccountPassword: 'O_Romeo_Romeo!'

compliance-tests:
  extends: .xmpp-interop-tests
  stage: test
  dependencies:
    - maven-build
  before_script: "./launch-my-server.sh"
```

Note the use of the `host` option to direct the test runner to resolve `shakespeare.lit` as the locally running server.
